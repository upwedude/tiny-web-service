import { assertEquals } from "https://deno.land/std/testing/asserts.ts";
import { ServerRequest } from "https://deno.land/std/http/server.ts";
import { BufWriter } from "https://deno.land/std/io/bufio.ts";
import htm from "https://cdn.pika.dev/htm@^3.0.2";
import { h } from "https://cdn.pika.dev/preact@^10.3.0";
import { renderToString } from "https://cdn.pika.dev/preact-render-to-string@^5.1.4";
import { handleIndex } from "../handlers/handlndex.js";
import { handleLogin } from "../handlers/api.js";
import { handleStatic, isStatic } from "../handlers/static.js";
import { Ablums } from "../components/ablums.js";
import { AjaxCall } from "../components/ajaxCall.js";
import { App } from "../components/app.js";
import { Error } from "../components/error.js";
import { Main } from "../components/main.js";
import { Setup } from "../components/setup.js";

const { Buffer } = Deno;
const html = htm.bind(h);
const buf = new Buffer();
const bufw = new BufWriter(buf);
const baseRequest = new ServerRequest();
baseRequest.w = bufw;

Deno.test("/ get", () => {
  const actual = handleIndex(baseRequest);
  const result =
    '<html><head><meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" /><meta charset="utf-8" /><link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" /><link rel="stylesheet" type="text/css" href="css/main.css" /><title>Tiny web service</title><script src="components/client.js" type="module"></script></head><body><div id="app"><section><article><h1></h1><div id="backgroundAjax" style="display:block;"><div class="spinner-border text-warning" role="status" id="ajaxLoader"><span class="sr-only">Loading...</span></div></div></article></section></div></body></html>';
  const actualResult = new TextDecoder("utf-8").decode(actual.body);
  assertEquals(actualResult, result);
});

Deno.test("/api/login get", async () => {
  const actual = await handleLogin(baseRequest);
  const actualResult = JSON.parse(new TextDecoder("utf-8").decode(actual.body));
  assertEquals(actualResult.expires_in, 3600);
});

Deno.test("isStatic found", () => {
  const actual = isStatic("/css/main.css");
  assertEquals(actual, "/css/main.css");
});

Deno.test("isStatic notFound", () => {
  const actual = isStatic("/js/index.js");
  assertEquals(actual, undefined);
});

Deno.test("handleStatic found", async () => {
  const filePath = "/css/main.css";
  baseRequest.url = filePath;
  const actual = await handleStatic(baseRequest);
  const actualResult = new TextDecoder("utf-8").decode(actual.body);
  const file = await Deno.readFile(`${Deno.cwd()}/${filePath}`);
  const result = new TextDecoder("utf-8").decode(file);
  baseRequest.url = undefined;
  assertEquals(actualResult, result);
  assertEquals(actual.status, 200);
});

Deno.test("handleStatic notFound", async () => {
  const result = { error: "No such file" };
  const filePath = "/css/other.css";
  baseRequest.url = filePath;
  const actual = await handleStatic(baseRequest);
  const actualResult = JSON.parse(new TextDecoder("utf-8").decode(actual.body));
  baseRequest.url = undefined;
  assertEquals(actualResult, result);
  assertEquals(actual.status, 404);
});

Deno.test("Ablums empty", () => {
  const acutal = Ablums({ passIn: [] });
  const result = [];
  assertEquals(acutal, result);
});

Deno.test("Ablums full", () => {
	const props = [{
		name: "me",
		artists: [{
			url: "url",
		}],
		images: [
			"img",
		],
	}];
	const acutalString = renderToString(html`<${Ablums} passIn=${props}/>`);
	const result = '<div class="card"><img class="card-img-top myCardImg" alt="ablum image" /><div class="card-body"><p class="card-text"> - me</p></div></div>'

	assertEquals(acutalString, result);
});

Deno.test("AjaxCall on", () => {
	const result = '<div id="backgroundAjax" style="display:block;"><div class="spinner-border text-warning" role="status" id="ajaxLoader"><span class="sr-only">Loading...</span></div></div>';
	const props = {
		on: true
	}
	const acutalString = renderToString(html`<${AjaxCall} on=${props}/>`);

	assertEquals(acutalString, result);
});

Deno.test("AjaxCall off", () => {
	const result = `<div id="backgroundAjax" style="display:block;"><div class="spinner-border text-warning" role="status" id="ajaxLoader"><span class="sr-only">Loading...</span></div></div>`;
	const props = {
		on: false
	}
	const acutalString = renderToString(html`<${AjaxCall} on=${props}/>`);
	assertEquals(acutalString, result);
});

Deno.test("App", () => {
	const result = "<section><article><h1></h1><div id=\"backgroundAjax\" style=\"display:block;\"><div class=\"spinner-border text-warning\" role=\"status\" id=\"ajaxLoader\"><span class=\"sr-only\">Loading...</span></div></div></article></section>";
	const acutalString = renderToString(html`<${App}/>`);
	assertEquals(acutalString, result);
});

Deno.test("Error string", () => {
	const result = '<h1>hi</h1>';
	const props = "hi";
	const acutalString = renderToString(html`<${Error} showError=${props}/>`);
	assertEquals(acutalString, result);
});

Deno.test("Error array", () => {
	const result = '<h1>foobar</h1>';
	const props = ["foo", "bar"];
	const acutalString = renderToString(html`<${Error} showError=${props}/>`);
	assertEquals(acutalString, result);
});

Deno.test("Error object", () => {
	const result = '<h1><undefined></undefined></h1>';
	const props = {
		err: "help"
	};
	const acutalString = renderToString(html`<${Error} showError=${props}/>`);
	assertEquals(acutalString, result);
});

Deno.test("Error string", () => {
	const result = "<form><div class=\"form-group\"><label for=\"ablumSearchInput\">Ablum search</label><input type=\"text\" class=\"form-control\" id=\"ablumSearchInput\" aria-describedby=\"ablumSearch\" /></div><button type=\"submit\" class=\"btn btn-primary\">Submit</button></form>";
	const props = {
		token: 123,
		tokenExpire: 3600
	};
	const acutalString = renderToString(html`<${Main} tokenPass=${props.token} expire=${props.tokenExpire}/>`);
	assertEquals(acutalString, result);
});

Deno.test("Setup", () => {
	const result = "<article><h1></h1><div id=\"backgroundAjax\" style=\"display:block;\"><div class=\"spinner-border text-warning\" role=\"status\" id=\"ajaxLoader\"><span class=\"sr-only\">Loading...</span></div></div></article>"
	const acutalString = renderToString(html`<${Setup}/>`);
	assertEquals(acutalString, result);
});