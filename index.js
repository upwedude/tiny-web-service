import { serve } from "https://deno.land/std/http/server.ts";
import { handleIndex } from "./handlers/handlndex.js";
import { handleStatic, isStatic } from "./handlers/static.js";
import { handleLogin } from "./handlers/api.js";

export async function createServer() {
  const myServe = serve({ port: 8000 });

  for await (const request of myServe) {
    switch (request.url) {
      case "/":
        handleIndex(request);
        break;
      case isStatic(request.url):
        handleStatic(request);
        break;
      case "/api/login":
        handleLogin(request);
        break;
    }
  }
  return myServe;
}

if (import.meta.main) {
  createServer();
}
