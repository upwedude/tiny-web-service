import htm from "https://cdn.pika.dev/htm@^3.0.2";
import { h } from "https://cdn.pika.dev/preact@^10.3.0";

const html = htm.bind(h);

export function Error(props) {
  return (
    html`<h1>${props.showError}</h1>`
  );
}
