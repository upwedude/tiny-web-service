import htm from "https://cdn.pika.dev/htm@^3.0.2";
import { h } from "https://cdn.pika.dev/preact@^10.3.0";
import { useState } from "https://cdn.pika.dev/preact@^10.3.0/hooks";
import { Ablums } from "./ablums.js";

const html = htm.bind(h);

export function Main(props) {
  const [ablums, setAblums] = useState("");
  const [foundAblums, setFoundAblums] = useState([]);

  async function searcher(event) {
    event.preventDefault();
    if (ablums != "") {
      const responseToken = await fetch(
        `https://api.spotify.com/v1/search?q=${ablums}&type=album`,
        {
          method: "get",
          headers: {
            "Authorization": `Bearer ${props.tokenPass}`,
          },
        },
      );
      if (responseToken.status == 200) {
        const responeData = await responseToken.json();
        setFoundAblums(responeData.albums.items);
      }
    }
  }

  return (
    html`<form onSubmit=${(event) => searcher(event)}>
      <div class="form-group">
        <label for="ablumSearchInput">Ablum search</label>
        <input type="text" class="form-control" id="ablumSearchInput" aria-describedby="ablumSearch" onChange=${(
      event,
    ) => setAblums(event.target.value)}/>
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
      ${
      foundAblums.length !== 0 ? html`<${Ablums} passIn=${foundAblums} />` : ""
    }
    </form>`
  );
}
