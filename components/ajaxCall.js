import htm from "https://cdn.pika.dev/htm@^3.0.2";
import { h } from "https://cdn.pika.dev/preact@^10.3.0";

const html = htm.bind(h);

export function AjaxCall(props) {
  const displayType = props.on ? "block" : "none";

  return (
    html`
    <div id="backgroundAjax" style="display:${displayType};">
      <div className="spinner-border text-warning" role="status" id="ajaxLoader">
        <span className="sr-only">Loading...</span>
      </div>
    </div>`
  );
}
