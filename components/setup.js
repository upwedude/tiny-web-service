import htm from "https://cdn.pika.dev/htm@^3.0.2";
import { h } from "https://cdn.pika.dev/preact@^10.3.0";
import { useState, useEffect } from "https://cdn.pika.dev/preact@^10.3.0/hooks";
import { Main } from "./main.js";
import { Error } from "./error.js";
import { AjaxCall } from "./ajaxCall.js";

const html = htm.bind(h);

export function Setup() {
  const [stateObject, setStateObject] = useState(false);
  const [token, setToken] = useState("");
  const [tokenExpire, setTokenExpire] = useState(-1);
  const [errorMessage, setErrorMessage] = useState("");

  async function login() {
    const response = await fetch(`http://localhost:8000/api/login`);
    const responeData = await response.json();

    if (response.status == 200) {
      setToken(responeData.access_token);
      setTokenExpire(responeData.expires_in);
    } else {
      setErrorMessage(responeData.error);
    }
    setStateObject(true);
  }

  useEffect(() => {
    login();
  }, []);

  return (
    html`
    <article>
      ${
      token
        ? html`<${Main} tokenPass=${token} expire=${tokenExpire} />`
        : html`<${Error} showError=${errorMessage} />`
    }
      <${AjaxCall} on=${!stateObject} />
    </article>`
  );
}
