import htm from "https://cdn.pika.dev/htm@^3.0.2";
import { h } from "https://cdn.pika.dev/preact@^10.3.0";
import { Setup } from "./setup.js";

const html = htm.bind(h);

export function App() {
  return (
    html`
    <section>
      <${Setup}/>
    </section>`
  );
}
