import htm from "https://cdn.pika.dev/htm@^3.0.2";
import { h } from "https://cdn.pika.dev/preact@^10.3.0";

const html = htm.bind(h);

export function Ablums(props) {
  return (
    props.passIn.map((value) =>
      html`<div class="card">
        <img src="${
        value.images[0].url
      }" class="card-img-top myCardImg" alt="ablum image"/>
        <div class="card-body">
          <p class="card-text">${value.artists[0].name} - ${value.name}</p>
        </div>
      </div>`
    )
  );
}
