import htm from "https://cdn.pika.dev/htm@^3.0.2";
import { h, hydrate } from "https://cdn.pika.dev/preact@^10.3.0";
import { App } from "./app.js";

const html = htm.bind(h);

hydrate(html`<${App} />`, document.getElementById("app"));
