import htm from "https://cdn.pika.dev/htm@^3.0.2";
import { h } from "https://cdn.pika.dev/preact@^10.3.0";
import { renderToString } from "https://cdn.pika.dev/preact-render-to-string@^5.1.4";
import { App } from "../components/app.js";

const html = htm.bind(h);
const body = renderToString(html`
  <html>
    <head>
      <meta charset="utf-8"/>
      <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
      <meta charset="utf-8"/>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"/>
      <link rel="stylesheet" type="text/css" href="css/main.css" />
      <title>Tiny web service</title>
      <script src="components/client.js" type="module"></script>
    </head>
    <body>
      <div id="app">
        <${App} />
      </div>
    </body>
  </html>
`);

export function handleIndex(request) {
  const responseObject = {
    status: 200,
    body: body,
  };

  request.respond(responseObject);
  return responseObject;
}
