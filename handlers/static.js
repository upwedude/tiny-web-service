import { extname } from "https://deno.land/std/path/mod.ts";
import { contentType } from "https://deno.land/x/media_types/mod.ts";

export async function handleStatic(request) {
  const responseObject = new Response();
  const decoder = new TextDecoder("utf-8");
  try {
    const filePath = `${Deno.cwd()}/${request.url}`;
    const responseType = extname(filePath);
    const fileStat = await Deno.stat(filePath);
    const file = await Deno.readFile(filePath);

    if (!fileStat.isFile) {
      responseObject.body = new TextEncoder().encode(JSON.stringify({
        error: "No such file",
      }));
      responseObject.status = 404;
    }

    responseObject.headers.set("Content-Length", String(fileStat.size));
    responseObject.headers.set("Content-Type", contentType(responseType));
    responseObject.headers.set("Last-Modified", fileStat.mtime.toUTCString());
    responseObject.type = responseType;
    responseObject.body = new TextEncoder().encode(decoder.decode(file));
    responseObject.status = 200;

    request.respond(responseObject);
  } catch (err) {
    responseObject.body = new TextEncoder().encode(JSON.stringify({
      error: "No such file",
    }));
    responseObject.status = 404;
  }
  return responseObject;
}

export function isStatic(urlPath) {
  const staticTypes = ["/css", "/components"];
  for (const type of staticTypes) {
    if (urlPath.includes(type)) {
      return urlPath;
    }
  }
}
