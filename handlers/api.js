export async function handleLogin(request) {
  const responseObject = new Response();
  const params = new URLSearchParams();
  try {
    const clientId = Deno.env.get("CLIENT_ID");
    const clientSecret = Deno.env.get("CLIENT_SECRET");

    responseObject.headers.set("Content-Type", "application/json");
    responseObject.type = "json";
    params.append("grant_type", "client_credentials");

    const responseToken = await fetch(
      "https://accounts.spotify.com/api/token",
      {
        method: "POST",
        body: params,
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          "Authorization": "Basic " +
            window.btoa(clientId + ":" + clientSecret),
        },
      },
    );
    const responeData = await responseToken.json();

    if (responseToken.status == 200) {
      responseObject.status = 200;
      responseObject.body = new TextEncoder().encode(
        JSON.stringify(responeData),
      );
    } else {
      responseObject.status = 500;
      responseObject.body = new TextEncoder().encode(
        JSON.stringify({ error: responeData }),
      );
    }
  } catch (err) {
    responseObject.status = 500;
    responseObject.body = new TextEncoder().encode(
      JSON.stringify({ error: err }),
    );
  }

  request.respond(responseObject);
  return responseObject;
}
